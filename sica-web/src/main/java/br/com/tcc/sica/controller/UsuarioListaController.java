package br.com.tcc.sica.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.tcc.sica.data.BarragemData;
import br.com.tcc.sica.data.UsuarioData;
import br.com.tcc.sica.model.Barragem;
import br.com.tcc.sica.model.Usuario;

//The @Model stereotype is a convenience mechanism to make this a request-scoped bean that has an
//EL name
//Read more about the @Model stereotype in this FAQ:
//http://www.cdi-spec.org/faq/#accordion6
@Model
@SessionScoped
public class UsuarioListaController implements Serializable {

	private static final long serialVersionUID = 1L;

	// alterado nome
	@Inject
	private UsuarioData usuario;

	@Inject
	private BarragemData barragem;

	Usuario newUsuario = new Usuario();
	//Barragem newBarragem = new Barragem();

//	UsuarioData user = new UsuarioData();

	private List<Barragem> barragens = new ArrayList<Barragem>();
	private List<Usuario> usuarios = new ArrayList<Usuario>();

	private String nomeResp;
	private String nomeBarragem;

	public String getNomeBarragem() {
		return nomeBarragem;
	}

	public String getNomeResp() {
		return nomeResp;
	}

	@Produces
	@Named
	public Usuario getNewUsuario() {
		return newUsuario;
	}
	

	

	public List<Usuario> getInsumos() {
		return usuarios;
	}

	public List<Usuario> getUsuarios() {

		return usuarios = usuario.findAllOrderedByName();

	}
	


}
