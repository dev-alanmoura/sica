/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.tcc.sica.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.omnifaces.util.Messages;

import br.com.tcc.sica.data.UsuarioData;
import br.com.tcc.sica.model.Usuario;
import br.com.tcc.sica.service.UsuarioRegistro;

//The @Model stereotype is a convenience mechanism to make this a request-scoped bean that has an
//EL name
//Read more about the @Model stereotype in this FAQ:
//http://www.cdi-spec.org/faq/#accordion6
@Model
@SessionScoped
public class UsuarioController {

	@Inject
	private FacesContext facesContext;

	@Inject
	private UsuarioRegistro memberRegistration;

	private Usuario newUser;
	private String tipoAcesso;
	Usuario usuario = new Usuario();

	@Inject
	UsuarioData usr = new UsuarioData();

	@Inject
	private UsuarioData userDT;

	List<Usuario> usuarios = new ArrayList<Usuario>();

	private String autorId;

	public void setAutorId(String autorId) {
		this.autorId = autorId;
	}

	public String getAutorId() {
		return autorId;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

//	public List<Usuario> getAutores() {
//		return new DAO<Usuario>(Usuario.class).listaTodos();
//	}

	public void carregaUsuarios() {

		usuarios = userDT.findAllOrderedByName();
	}

	@Produces
	@Named
	public String getTipoAcesso() {
		return tipoAcesso;
	}

	public void setTipoAcesso(String tipoAcesso) {
		this.tipoAcesso = tipoAcesso;
	}

	@Produces
	@Named
	public Usuario getNewUser() {
		return newUser;
	}

	public void registerUser() {
		try {
			memberRegistration.registerUser(newUser);
			Messages.addGlobalInfo("Usuario cadastrado com sucesso");
			initNewMember();

		} catch (Exception e) {
			String errorMessageUser = getRootErrorMessage(e);
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessageUser, "Falha ao salvar dados");
			facesContext.addMessage(null, m);
		}
	}

	public void removeUser() {
		try {

			memberRegistration.removeUser(newUser);

			initNewMember();

		} catch (Exception e) {
			String errorMessageUser = getRootErrorMessage(e);
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessageUser, "Falha ao salvar dados");
			facesContext.addMessage(null, m);
		}
	}

	public String loginUsuario() {

//		newUser.setUser("admoura");
//		newUser.setPassword("alan");

		newUser = memberRegistration.getUsuario(newUser.getUser(), newUser.getPassword());

		FacesContext context = FacesContext.getCurrentInstance();


		if (newUser == null) {

			Messages.addGlobalInfo("Usuário não encontrado!");
			newUser = new Usuario();
			return null;
		} else {

			context.getExternalContext().getSessionMap().put("usuarioLogado", this.newUser);
			

			return "Home?faces-redirect=true";
		}
	}
	
	public String deslogar() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().remove("usuarioLogado", newUser);
		return "login?faces-redirect=true";
	}

	@PostConstruct
	public void initNewMember() {

		newUser = new Usuario();

	}

	private String getRootErrorMessage(Exception e) {
		// Default to general error message that registration failed.
		String errorMessage = "Falha ao gravar, veja os logs do sistema";
		if (e == null) {
			// This shouldn't happen, but return the default messages
			return errorMessage;
		}

		// Start with the exception and recurse to find the root cause
		Throwable t = e;
		while (t != null) {
			// Get the message from the Throwable class instance
			errorMessage = t.getLocalizedMessage();
			t = t.getCause();
		}
		// This is the root cause message
		return errorMessage;
	}
}
