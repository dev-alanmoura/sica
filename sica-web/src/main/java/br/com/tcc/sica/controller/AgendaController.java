package br.com.tcc.sica.controller;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.omnifaces.util.Messages;

import br.com.tcc.sica.data.AgendaData;
import br.com.tcc.sica.model.Agenda;
import br.com.tcc.sica.model.StatusAgenda;

@Model
@SessionScoped
public class AgendaController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private FacesContext facesContext;

	private Agenda agenda = new Agenda();

	private List<Agenda> agendas = new ArrayList<Agenda>();

	@Inject
	private AgendaData agendaData = new AgendaData();

	@Produces
	@Named
	public Agenda getAgenda() {
		return agenda;
	}

	public List<Agenda> getAgendas() {
		return agendas;
	}

	@PostConstruct
	public void carregaDatasDisponiveis() {

		agendas = agendaData.listaDatasDisponiveis();

	}

	@Transactional
	public void inserirAgenda() throws Exception {

		try {

			System.out.println("Inserindo agenda:::::::::::::::" + agenda.getDataAgendamento() + "status "
					+ agenda.getStatusAgendamento());

			agendaData.cadastroAgenda(agenda);

			Messages.addGlobalInfo("Data inserida com sucesso na agenda");
			limpaCampos();

		} catch (Exception e) {
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cadastro",
					"Erro ao cadastrar" + agenda.getDataAgendamento().toString()));
			String message = e.getMessage();
			System.out.println(message);

		}

		carregaDatasDisponiveis();

	}

	public void limpaCampos() {
		Agenda limpaAgenda = new Agenda();
		agenda = limpaAgenda;

	}

	private static Calendar parseData(String data) {
		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(data);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			return calendar;
		} catch (ParseException e) {
			throw new IllegalArgumentException(e);
		}
	}

}
