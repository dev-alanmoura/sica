package br.com.tcc.sica.controller;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.omnifaces.util.Messages;

import br.com.tcc.sica.data.AgendaData;
import br.com.tcc.sica.data.AgendamentoData;
import br.com.tcc.sica.data.UsuarioData;
import br.com.tcc.sica.model.Agenda;
import br.com.tcc.sica.model.Agendamento;
import br.com.tcc.sica.model.Barragem;
import br.com.tcc.sica.model.Insumo;
import br.com.tcc.sica.model.Usuario;

//The @Model stereotype is a convenience mechanism to make this a request-scoped bean that has an
//EL name
//Read more about the @Model stereotype in this FAQ:
//http://www.cdi-spec.org/faq/#accordion6
@Model
@SessionScoped
public class AgendamentoController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private FacesContext facesContext;

	private Agendamento agendamento = new Agendamento();

	private Insumo insumoController = new Insumo();
	private Agenda agendaController = new Agenda();
	private Usuario usuarioController = new Usuario();
	private Barragem barragemController = new Barragem();

	@Inject
	private AgendamentoData agendamentoData = new AgendamentoData();

	@Inject
	private AgendaData agendaData = new AgendaData();

	UsuarioData userData = new UsuarioData();

	private List<Insumo> insumos = new ArrayList<Insumo>();

	private List<Usuario> usuarios = new ArrayList<Usuario>();

	private List<Agenda> agendas = new ArrayList<Agenda>();

	private List<Barragem> barragens = new ArrayList<Barragem>();

	private List<Agendamento> agendamentos = new ArrayList<Agendamento>();

	private int idResponsavel;
	private int idBarragem;

	public int getIdBarragem() {
		return idBarragem;
	}

	public int getIdResponsavel() {
		return idResponsavel;
	}

	public List<Agendamento> getAgendamentos() {
		return agendamentos;
	}

	@Produces
	@Named
	public Agendamento getAgendamento() {
		return agendamento;
	}

	@Produces
	@Named(value = "insumoBean")
	public Insumo getInsumoController() {
		return insumoController;
	}

	@Produces
	@Named(value = "usuarioBean")
	public Usuario getUsuarioController() {
		return usuarioController;
	}

	@Produces
	@Named(value = "agendaBean")
	public Agenda getAgendaController() {
		return agendaController;
	}

	@Produces
	@Named(value = "barragemBean")
	public Barragem getBarragemController() {
		return barragemController;
	}

	public List<Insumo> getInsumos() {
		return insumos;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public List<Agenda> getAgendas() {
		return agendas;
	}

	public List<Barragem> getBarragens() {
		return barragens;
	}

	@PostConstruct
	public void carregaTodos() {

		agendas = agendamentoData.listaDatasDisponiveis();

		insumos = agendamentoData.listaInsumos();

		usuarios = agendamentoData.listaUsuarios();

		barragens = agendamentoData.listaBarragens();

		 agendamentos = agendamentoData.listaAgendamentos();
		 
	}
	
	public String getDataFormatada(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println(":::::::::::::::::::::::::DATA" + agendaController.getDataAgendamento().toString());
        return sdf.format(agendaController.getDataAgendamento().toString());
}
	public static String formatDate(String string) {
	     DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");   
	     return dateFormat.format(string);
	}
 
	
	public void novo() {

		initNewInsumoAgenda();

	}

//
	private void initNewInsumoAgenda() {
		insumoController = new Insumo();
		usuarioController = new Usuario();
		agendaController = new Agenda();
		barragemController = new Barragem();

	}

	public void carregaInsumosForm(Agendamento agendamento) {

		System.out.println("enviando insumo para formulario");

		this.agendamento = agendamento;

	}
 
	private  Calendar parseData(String data) { 
		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(agendaController.getDataAgendamento().toString());
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			return calendar;
		} catch (ParseException e) {
			throw new IllegalArgumentException(e);
		}
	}
	
	@Transactional
	public void cadastraAgendamento() {

		 System.out.println(":::::::::::::::::::" + this.agendamento.getAgenda().getId());

		try {

			if (this.agendamento.getId() == null ) {
				agendamentoData.registraAgendamento(agendamento);

				agendamentoData.atualizaAgendaPorId(this.agendamento.getAgenda().getId(),
						this.agendamento.getAgenda().getDataAgendamento().toString());

				Messages.addGlobalInfo("Agendamento realizado com sucesso");
			
				novo();
				agendamentoData.listaAgendamentos();
			} else {
				agendamentoData.atualizaAgendamento(agendamento);
				Messages.addGlobalInfo("Agendamento atualizado com sucesso");
				
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			Messages.addFlashGlobalError("Erro ao agendar");
		}
		
	}

	@Transactional
	public void removeAgendamento(Agendamento agendamento) {

		try {

			agendamentoData.removeAgendamento(agendamento);
			Messages.addGlobalInfo("Agendamento removido com sucesso");
			agendamentoData.listaAgendamentos();
			novo();

		} catch (Exception e) {
			e.printStackTrace();

		}

	}
	
	

	

	private String getRootErrorMessage(Exception e) {
		// Default to general error message that registration failed.
		String errorMessage = "Registration failed. See server log for more information";
		if (e == null) {
			// This shouldn't happen, but return the default messages
			return errorMessage;
		}

		// Start with the exception and recurse to find the root cause
		Throwable t = e;
		while (t != null) {
			// Get the message from the Throwable class instance
			errorMessage = t.getLocalizedMessage();
			t = t.getCause();
		}
		// This is the root cause message
		return errorMessage;
	}

}
