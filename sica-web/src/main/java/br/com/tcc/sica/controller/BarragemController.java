package br.com.tcc.sica.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.omnifaces.util.Messages;

import br.com.tcc.sica.data.BarragemData;
import br.com.tcc.sica.data.InsumoData;
import br.com.tcc.sica.data.UsuarioData;
import br.com.tcc.sica.model.Barragem;
import br.com.tcc.sica.model.Insumo;
import br.com.tcc.sica.model.Usuario;
import br.com.tcc.sica.service.BarragemRegistro;

//The @Model stereotype is a convenience mechanism to make this a request-scoped bean that has an
//EL name
//Read more about the @Model stereotype in this FAQ:
//http://www.cdi-spec.org/faq/#accordion6
@Model
@SessionScoped
public class BarragemController implements Serializable {
 
	private static final long serialVersionUID = 1L;

	@Inject
	private FacesContext facesContext;

	@Inject
	private BarragemData barragem;

	Barragem newBarragem = new Barragem();

	private List<Barragem> barragens = new ArrayList<Barragem>();
	
	
	
	
	@Produces
	@Named
	public Barragem getNewBarragem() {
		return newBarragem;
	}
	
	
	private String nomeBarragem;

	public String getNomeBarragem() {
		return nomeBarragem;
	}


	@Transactional
	public void registraBarragem() throws Exception {
		try {

			barragem.registraBarragem(newBarragem);

			Messages.addGlobalInfo("Cadastro realizado com sucesso!");
			
			initNewBarragem();
			
		} catch (Exception e) {
			String errorMessage = getRootErrorMessage(e);
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, "Registration Unsuccessful");
			facesContext.addMessage(null, m);
		}
	}

	private void initNewBarragem() {
		newBarragem = new Barragem();

	}

	private String getRootErrorMessage(Exception e) {
		// Default to general error message that registration failed.
		String errorMessage = "Registration failed. See server log for more information";
		if (e == null) {
			// This shouldn't happen, but return the default messages
			return errorMessage;
		}

		// Start with the exception and recurse to find the root cause
		Throwable t = e;
		while (t != null) {
			// Get the message from the Throwable class instance
			errorMessage = t.getLocalizedMessage();
			t = t.getCause();
		}
		// This is the root cause message
		return errorMessage;
	}

	public List<Barragem> getBarragems() {

		return barragens = barragem.findAllOrderedByName();
	}

}
