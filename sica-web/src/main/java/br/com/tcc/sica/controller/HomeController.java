package br.com.tcc.sica.controller;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.omnifaces.util.Messages;

import br.com.tcc.sica.data.AgendaData;
import br.com.tcc.sica.data.AgendamentoData;
import br.com.tcc.sica.data.InsumoRepository;
import br.com.tcc.sica.model.Agenda;
import br.com.tcc.sica.model.StatusAgenda;

@Model
@SessionScoped
public class HomeController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private FacesContext facesContext;

	@Inject
	private AgendamentoData agData = new AgendamentoData();

	private Integer countItem;
	private Integer countItemsAndamento;
	private Integer countItemsFalha;
	private Integer countItemBarragemAlerta;

	@Produces
	@Named
	public Integer getCountItemsFalha() {
		return countItemsFalha;
	}

	public void setCountItemsFalha(Integer countItemsFalha) {
		this.countItemsFalha = countItemsFalha;
	}

	@Produces
	@Named
	public Integer getCountItemBarragemAlerta() {
		return countItemBarragemAlerta;
	}

	public void setCountItemBarragemAlerta(Integer countItemBarragemAlerta) {
		this.countItemBarragemAlerta = countItemBarragemAlerta;
	}

	@Produces
	@Named
	public Integer getCountItemsAndamento() {
		return countItemsAndamento;
	}

	public void setCountItemsAndamento(Integer countItemsAndamento) {
		this.countItemsAndamento = countItemsAndamento;
	}

	@Produces
	@Named
	public Integer getCountItem() {
		return countItem;
	}

	public void setCountItem(Integer countItem) {
		this.countItem = countItem;
	}

	@PostConstruct
	public void carregaDatasDisponiveis_() {

		countItem = agData.contaAgendamentos();
		countItemsAndamento = agData.contaAgendamentosAndamento();
		countItemsFalha = agData.contaEquipamentosFalha();
		countItemBarragemAlerta = agData.contaBarragensComAlerta();
	}

}
