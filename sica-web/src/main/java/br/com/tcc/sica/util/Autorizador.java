package br.com.tcc.sica.util;

import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.inject.Inject;

import br.com.tcc.sica.data.MonitorData;
import br.com.tcc.sica.model.Usuario;

public class Autorizador implements PhaseListener {

	@Inject
	MonitorData mon = new MonitorData();

	@Override
	public void afterPhase(PhaseEvent event) {

		FacesContext context = event.getFacesContext();
 
		String nomePagina = context.getViewRoot().getViewId();

		System.out.println("Pagina:::::::::::::" + nomePagina);

		if ("/login.xhtml".equals(nomePagina)) {

			return;
		}

		Usuario usuario = (Usuario) context.getExternalContext().getSessionMap().get("usuarioLogado");

		if (usuario != null) {

			return;
		}

		NavigationHandler handler = context.getApplication().getNavigationHandler();
		handler.handleNavigation(context, null, "/login?faces-redirect=true");

		context.renderResponse();

	}

	@Override
	public void beforePhase(PhaseEvent event) {
			
		 
		
	}

	@Override
	public PhaseId getPhaseId() {

		return PhaseId.RESTORE_VIEW;
	}

}
