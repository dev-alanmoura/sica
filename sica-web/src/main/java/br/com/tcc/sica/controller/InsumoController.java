package br.com.tcc.sica.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.omnifaces.util.Messages;

import br.com.tcc.sica.data.InsumoData;
import br.com.tcc.sica.data.UsuarioData;
import br.com.tcc.sica.model.Insumo;
import br.com.tcc.sica.model.Usuario;

//The @Model stereotype is a convenience mechanism to make this a request-scoped bean that has an
//EL name
//Read more about the @Model stereotype in this FAQ:
//http://www.cdi-spec.org/faq/#accordion6
@Model
@SessionScoped
public class InsumoController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private FacesContext facesContext;

	@Inject
	private InsumoData insumo;

	Insumo newInsumo = new Insumo();

	UsuarioData user = new UsuarioData();

	private List<Insumo> insumos = new ArrayList<Insumo>();

	@Produces
	@Named
	public Insumo getNewInsumo() {
		return newInsumo;
	}

	public List<Insumo> getInsumos() {
		return insumos;
	}

	@PostConstruct
	public void carregaInsumos() {

		insumos = insumo.findAllOrderedByName();

	}

	public String carregaInsumoForm(Insumo insumo) {
		// carregar dados para editar

		this.newInsumo = insumo;
		return "formCadastroInsumo?faces-redirect=true";

	}

	@Transactional
	public void registraInsumo() throws Exception {
		try {

			System.out.println("IDDDDDDD :::::::::::::::" + this.newInsumo.getId());

			if (this.newInsumo.getId() == null) {
				insumo.registraInsumo(newInsumo);
				Messages.addGlobalInfo("Insumo Cadastrado com sucesso!");
				initNewInsumo();
				
			} else {
				insumo.atualizaInsumo(newInsumo);
				Messages.addGlobalInfo("Insumo Alterado com sucesso!");
				initNewInsumo();
			}

//			initNewInsumo();
		} catch (Exception e) {
			String errorMessage = getRootErrorMessage(e);
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, "Erro ao realizar operação");
			facesContext.addMessage(null, m);
		}

		carregaInsumos();
	}

	public void novoInsumo() {

		initNewInsumo();

	}

	public void removeInsumo(Insumo _insumoBean) throws Exception {
		try {

			insumo.removeInsumo(_insumoBean);
			initNewInsumo();
			Messages.addGlobalInfo("Insumo removido com sucesso!");

		} catch (Exception e) {
			String errorMessage = getRootErrorMessage(e);
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, "Erro ao remover");
			facesContext.addMessage(null, m);
		}

		carregaInsumos();
	}

	private void initNewInsumo() {
		newInsumo = new Insumo();

	}

//    @Produces
//    @Named
//	public List<Insumo> recuperaInsumos() {
//		
//	return insumos =  insumo.findAllOrderedByName();
//
//	
//	}

 
	public String editar(Insumo insumo) {
		
		
		ActionEvent event = null;
		newInsumo = (Insumo) event.getComponent().getAttributes().get("insumoSelecionado");

		return "formCadastroInsumo?faces-redirect=true";

	}

	public void carregaInsumosForm(Insumo insumo) {

		System.out.println("enviando insumo para formulario");
		this.newInsumo = insumo;

	}

	private String getRootErrorMessage(Exception e) {
		// Default to general error message that registration failed.
		String errorMessage = "Registration failed. See server log for more information";
		if (e == null) {
			// This shouldn't happen, but return the default messages
			return errorMessage;
		}

		// Start with the exception and recurse to find the root cause
		Throwable t = e;
		while (t != null) {
			// Get the message from the Throwable class instance
			errorMessage = t.getLocalizedMessage();
			t = t.getCause();
		}
		// This is the root cause message
		return errorMessage;
	}

}
