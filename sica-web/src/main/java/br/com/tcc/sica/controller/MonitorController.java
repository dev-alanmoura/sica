package br.com.tcc.sica.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.omnifaces.util.Messages;

import br.com.tcc.sica.data.AgendaData;
import br.com.tcc.sica.data.AgendamentoData;
import br.com.tcc.sica.data.MonitorData;
import br.com.tcc.sica.data.UsuarioData;
import br.com.tcc.sica.model.Agenda;
import br.com.tcc.sica.model.Agendamento;
import br.com.tcc.sica.model.Barragem;
import br.com.tcc.sica.model.Insumo;
import br.com.tcc.sica.model.Monitor;
import br.com.tcc.sica.model.Monitor_;
import br.com.tcc.sica.model.StatusAgenda;
import br.com.tcc.sica.model.Usuario;

//The @Model stereotype is a convenience mechanism to make this a request-scoped bean that has an
//EL name
//Read more about the @Model stereotype in this FAQ:
//http://www.cdi-spec.org/faq/#accordion6
@Model
@SessionScoped
public class MonitorController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private  static final String TIPO_ACESSO = "Manutencao";
	
	
	@Inject
	private FacesContext facesContext;

	// private Monitor monitor = new Monitor();

	@Inject
	MonitorData monitorData = new MonitorData();

	Usuario usuario = new Usuario();
	
	
	List<Monitor> monitoramentos = new ArrayList<Monitor>();

	@Produces
	@Named
	public List<Monitor> getMonitoramentos() {
		return monitoramentos;
	}

//	
//	@Produces
//	@Named
//    public Monitor getMonitor() {
//		return monitor;
//	}
	@Produces
	@Named
	public Usuario getUsuario() {
		return usuario;
	}

	@PostConstruct
	public void carregaMonitor() {

		monitoramentos = monitorData.listaMonitoramento();
		isUsuarioNormal();
	}
	
	public String verificaAcesso() {
		
		
		FacesContext context = FacesContext.getCurrentInstance();	 
		Usuario tipoAcesso = (Usuario) context.getExternalContext().getSessionMap().get("usuarioLogado");
		
		if (tipoAcesso.getTipoAcesso() != TIPO_ACESSO) {
			
			return "formMonitoramento.xhtml"; 
		} 
		
		return "/Home?faces-redirect=true"; 
		 
	}
	
	public void enviaALerta() {
		
		//lógica para enviar para os responsaveis
		
		
		Messages.addGlobalInfo("Alerta enviado com sucesso");
	}
	
	public String isUsuarioNormal(){
		Usuario p =(Usuario)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuarioLogado");
		if(p.getTipoAcesso() == "Manutencao") {
			return "/Home?faces-redirect=true"; 
		}
		
		return (p.getTipoAcesso());
		
		
		}
	
	

}
