package br.com.tcc.sica.test;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

//import org.junit.Test;

import br.com.tcc.sica.model.Agendamento;
import br.com.tcc.sica.model.Usuario;

public class TestJoin {

	//@Test
	public static void main(String[] args) {

		EntityManager em = entityManagerFactory.createEntityManager();
			
		em.getTransaction().begin();

		Usuario usuario = new Usuario();
		usuario.setId(1L);

		String jpql = "select m from Agendamento m join m.Usuario c where c = :pUsuario";
		
		Query query = em.createQuery(jpql);
		query.setParameter("pUsuario", usuario);

		List<Agendamento> resultados = query.getResultList();

		for (Agendamento agendamento : resultados) {
		//	System.out.println(agendamento.getBarragem().getNome());
		//	System.out.println(agendamento.getUsuario().getName());
		}

	}

	private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("primary");

	public EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}

}
