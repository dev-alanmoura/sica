package br.com.tcc.sica.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Agendamento.class)
public abstract class Agendamento_ {

	public static volatile SingularAttribute<Agendamento, Barragem> barragem;
	public static volatile SingularAttribute<Agendamento, String> tipo;
	public static volatile SingularAttribute<Agendamento, Insumo> insumo;
	public static volatile SingularAttribute<Agendamento, Usuario> usuario;
	public static volatile SingularAttribute<Agendamento, Long> id;
	public static volatile SingularAttribute<Agendamento, Agenda> agenda;
	public static volatile SingularAttribute<Agendamento, String> status;
	public static volatile SingularAttribute<Agendamento, String> descricao;

}

