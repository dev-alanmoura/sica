package br.com.tcc.sica.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TransactionRequiredException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import br.com.tcc.sica.model.Agenda;
import br.com.tcc.sica.model.Agenda_;
import br.com.tcc.sica.model.Agendamento;
import br.com.tcc.sica.model.Agendamento_;
import br.com.tcc.sica.model.Barragem;
import br.com.tcc.sica.model.Barragem_;
//import br.com.caelum.livraria.dao.JPAUtil;
import br.com.tcc.sica.model.Insumo;
import br.com.tcc.sica.model.Insumo_;
import br.com.tcc.sica.model.StatusAgenda;
import br.com.tcc.sica.model.Usuario;
import br.com.tcc.sica.model.UsuarioRest;
import br.com.tcc.sica.model.UsuarioRest_;
import br.com.tcc.sica.model.Usuario_;

@ApplicationScoped
public class AgendamentoData {

	@Inject
	private Logger log;

	@PersistenceContext
	private EntityManager em;

	private List<Insumo> insumos;
	private List<Usuario> usuarios;
	private List<Agenda> agendas;
	private List<Barragem> barragens;

	@Inject
	private Event<Insumo> insumoEventSrc;

	public Insumo findById(Long id) {
		return em.find(Insumo.class, id);
	}

	@Transactional
	public void registraAgendamento(Agendamento agendamento) {

		try {

			log.info("Gravando Agendamento::::::::::::::::");

			em.merge(agendamento);

		} catch (Exception e) {
			System.out.println("Erro ao gravar agendamento  >>>> " + e.getMessage());
		}

	}

	@Transactional
	public void atualizaAgendamento(Agendamento agendamento) {

		try {

			log.info("Atualizando agendamento: " + agendamento.getId());

			em.merge(agendamento);

		} catch (Exception e) {
			System.out.println("Erro ao gravar agendamento  >>>> " + e.getMessage());
		}

	}

	@Transactional
	public void removeAgendamento(Agendamento agendamento) throws TransactionRequiredException {

		try {

			em.remove(em.getReference(agendamento.getClass(), agendamento.getId()));

		} catch (Exception e) {
			System.out.println("Erro ao remover agendamento  >>>> " + e.getMessage());
		}

	}

	public List<Insumo> getInsumos() {
		return insumos;

	}

	@Transactional
	public List<Insumo> listaInsumos() {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Insumo> criteria = cb.createQuery(Insumo.class);
		Root<Insumo> insumo = criteria.from(Insumo.class);

		try {
			criteria.select(insumo).orderBy(cb.asc(insumo.get("nome")));

		} catch (Exception e) {
			// TODO: handle exception
		}

		return em.createQuery(criteria).getResultList();

	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	@Transactional
	public List<Usuario> listaUsuarios() {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Usuario> criteria = cb.createQuery(Usuario.class);
		Root<Usuario> usuario = criteria.from(Usuario.class);
		try {

			criteria.select(usuario).orderBy(cb.asc(usuario.get("name")));

		} catch (Exception e) {
			// TODO: handle exception
		}
		return em.createQuery(criteria).getResultList();
	}

	public List<Agenda> getAgendas() {
		return agendas;
	}

	@Transactional
	public List<Agenda> listaDatasDisponiveis() {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Agenda> criteria = cb.createQuery(Agenda.class);
		Root<Agenda> agendaD = criteria.from(Agenda.class);

		// ,
		// StatusAgenda.LIVRE)

		criteria.select(agendaD).where(cb.equal(agendaD.get("statusAgendamento"), StatusAgenda.LIVRE));
		criteria.select(agendaD).orderBy(cb.asc(agendaD.get("dataAgendamento")));
		// criteria.select(agendaD).where(cb.equal(agendaD.get("dataAgendamento")));

		return em.createQuery(criteria).getResultList();
	}

	public List<Barragem> getBarragens() {
		return barragens;
	}

	@Transactional
	public List<Barragem> listaBarragens() {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Barragem> criteria = cb.createQuery(Barragem.class);
		Root<Barragem> barragem = criteria.from(Barragem.class);
		try {

			criteria.select(barragem).orderBy(cb.asc(barragem.get("nome")));

		} catch (Exception e) {
			// TODO: handle exception
		}
		return em.createQuery(criteria).getResultList();
	}

	@Transactional
	public List<Agendamento> listaAgendamentos() {

		CriteriaBuilder cb = em.getCriteriaBuilder();

		CriteriaQuery<Agendamento> criteria = cb.createQuery(Agendamento.class);

		Root<Agendamento> agendamento = criteria.from(Agendamento.class);

		agendamento.fetch(Agendamento_.insumo, JoinType.INNER);
		agendamento.fetch(Agendamento_.usuario, JoinType.INNER);
		agendamento.fetch(Agendamento_.agenda, JoinType.INNER);
		agendamento.fetch(Agendamento_.barragem, JoinType.INNER);
		
	 
		return em.createQuery(criteria).getResultList();

	}

	@Transactional
	public List<Agendamento> listaAgendamentosRest() {

		CriteriaBuilder cb = em.getCriteriaBuilder();

		CriteriaQuery<Agendamento> criteria = cb.createQuery(Agendamento.class);

		Root<Agendamento> agendamento = criteria.from(Agendamento.class);

		agendamento.fetch(Agendamento_.insumo, JoinType.INNER);
		agendamento.fetch(Agendamento_.usuario.getName(), JoinType.INNER);
		agendamento.fetch(Agendamento_.agenda, JoinType.INNER);
		agendamento.fetch(Agendamento_.barragem, JoinType.INNER);

		return em.createQuery(criteria).getResultList();

	}

	public List<Agendamento> listaAgendamentoAndamento() {

		CriteriaBuilder cb = em.getCriteriaBuilder();

		CriteriaQuery<Agendamento> criteria = cb.createQuery(Agendamento.class);

		Root<Agendamento> agendamento = criteria.from(Agendamento.class);

		agendamento.fetch(Agendamento_.insumo, JoinType.INNER);
		agendamento.fetch(Agendamento_.agenda, JoinType.INNER);
		agendamento.fetch(Agendamento_.barragem, JoinType.INNER);
 		agendamento.fetch(Agendamento_.usuario.getName(), JoinType.INNER);

		return em.createQuery(criteria).getResultList();

	}

	public Integer contaAgendamentos() {

		long result = (Long) em.createQuery("select count(n) from Agendamento n").getSingleResult();

		return (int) result;
	}

	public Integer contaAgendamentosAndamento() {

		String status = "Andamento";

		long result = (Long) em.createQuery("select count(n) from Agendamento n where n.status = 'Andamento' ")
				.getSingleResult();

		return (int) result;
	}

	public Integer contaEquipamentosFalha() {

		
		
			long result = (Long) em.createQuery("select count(n) from Agendamento n where n.status = 'Falha' ")
					.getSingleResult();
			if(result > 0) {
				return (int) result;
			}else {
				return 0;
			}
				
	
		 
		
	}
	
	
	
	
	public List<Agendamento> listaAgendamentosSemUsuario() {

		
		
		List<Agendamento> agendamentos =  em.createQuery("select ag.id as codigo, ag.tipo, ag.status, ag.descricao from Agendamento ag ")
				.getResultList();
	 
			return agendamentos;

	 
	
}
	
	
	
	
	
	
	public void atualizaAgendaPorId(Long id, String data) {

		try {

			Agenda agenda = new Agenda();

			if (id != null) {
				agenda.setId(id);
				agenda.setStatusAgendamento(StatusAgenda.RESERVADO);
				agenda.setDataAgendamento(data);
				em.merge(agenda);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Integer contaBarragensComAlerta() {

		long result = (Long) em.createQuery("select count(n) from Monitor n where n.statusMonitora = 'Alerta' ")
				.getSingleResult();
		if(result > 0) {
			return (int) result;
		}else {
			return 0;
		}
	}
	
	private static Calendar parseData(String data) {
		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(data);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			return calendar;
		} catch (ParseException e) {
			throw new IllegalArgumentException(e);
		}
	}

}
