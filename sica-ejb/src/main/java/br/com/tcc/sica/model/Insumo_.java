package br.com.tcc.sica.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Insumo.class)
public abstract class Insumo_ {

	public static volatile SingularAttribute<Insumo, String> tipo;
	public static volatile SingularAttribute<Insumo, String> dataFabricacao;
	public static volatile SingularAttribute<Insumo, String> nome;
	public static volatile SingularAttribute<Insumo, Long> id;
	public static volatile SingularAttribute<Insumo, String> tag;
	public static volatile SingularAttribute<Insumo, String> descricao;

}

