/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.tcc.sica.service;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.management.monitor.Monitor;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class MonitorRegistro {

    @Inject
    private Logger log;

    @Inject
    private EntityManager em;

  
    public void gravaMonitor(br.com.tcc.sica.model.Monitor monitor_) throws Exception {
    	
    	try {
        	em.persist(monitor_);
			
		} catch (Exception e) {
			throw new Exception("Erro ao gravar Monitor" + e.getMessage());
		}
    	
    	
    }
    
	public List<Monitor> listaMonitoramento() {
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Monitor> criteria = cb.createQuery(Monitor.class);
		Root<Monitor> monitor = criteria.from(Monitor.class);

		criteria.select(monitor).orderBy(cb.asc(monitor.get("barragem")));
		System.out.println("#######################################" + em.createQuery(criteria).getResultList());
		
		
		return em.createQuery(criteria).getResultList();
	}
	
    

}
