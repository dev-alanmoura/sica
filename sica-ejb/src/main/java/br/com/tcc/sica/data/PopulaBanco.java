package br.com.tcc.sica.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.tcc.sica.model.Usuario;

public class PopulaBanco {

	@PersistenceContext
	public static EntityManager em;

	public static void main(String[] args) {
		
		gravaUsuario();
		


	}

	private static Usuario geraUsuario(String nome, String usuario_nome, String telefone, String tipoAcesso,
			String senha, String email) {

		Usuario usuario = new Usuario();

		usuario.setName(nome);
		usuario.setUser(usuario_nome);
		usuario.setEmail(email);
		usuario.setPassword(senha);
		usuario.setPhoneNumber(telefone);
		usuario.setTipoAcesso(tipoAcesso);

		return usuario;

	}
	
	
	public static void gravaUsuario() {
		
		Usuario usuario1 = geraUsuario("Dayane", "dayf", "11940025411", "Engenharia", "day123", "day@email.com");
		Usuario usuario2 = geraUsuario("Sueila", "saeila", "11940025411", "Engenharia", "sa123", "sa@email.com");
		Usuario usuario3 = geraUsuario("Bruno", "brmoura", "11940025411", "Manutenção", "br123", "br@email.com");
		Usuario usuario4 = geraUsuario("Wallace", "wallace", "11940025411", "Engenharia", "ws123", "ws@email.com");
		Usuario usuario5 = geraUsuario("Val", "val", "11940025411", "Engenharia", "val123", "val@email.com");
		Usuario usuario6 = geraUsuario("Deiton", "deiton", "11940025411", "Engenharia", "deiton23", "deiton@email.com");
		Usuario usuario7 = geraUsuario("Amilton", "milton", "11940025411", "Engenharia", "milton123",
				"milton@email.com");
		Usuario usuario8 = geraUsuario("Jorge", "orge", "11940025411", "Engenharia", "orge123", "orge@email.com");
		Usuario usuario9 = geraUsuario("Otavio", "otavio", "11940025411", "Engenharia", "otavio123",
				"otavio@email.com");
		Usuario usuario10 = geraUsuario("Assis", "dssis", "11940025411", "Engenharia", "dassis", "ssis@email.com");

		em.persist(usuario1);
		em.persist(usuario2);
		em.persist(usuario3);
		em.persist(usuario4);
		em.persist(usuario5);
		em.persist(usuario6);
		em.persist(usuario7);
		em.persist(usuario8);
		em.persist(usuario9);
		em.persist(usuario10);
	}
	
	
	public void geraBarragem() {

	}

	public void geraAgenda() {

	}

	public void geraInsumo() {

	}
	
	private static Calendar parseData(String data) {
		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(data);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			return calendar;
		} catch (ParseException e) {
			throw new IllegalArgumentException(e);
		}
	}

}
