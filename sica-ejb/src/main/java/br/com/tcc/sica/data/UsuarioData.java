/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.tcc.sica.data;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;

import br.com.tcc.sica.model.Insumo;
import br.com.tcc.sica.model.Usuario;
import br.com.tcc.sica.model.UsuarioRest;
import br.com.tcc.sica.model.UsuarioRest_;
import br.com.tcc.sica.model.Usuario_;

@ApplicationScoped
public class UsuarioData {

	@Inject
	private EntityManager em;
	
	
	public Usuario findById(Long id) {
		return em.find(Usuario.class, id);
	}

	public Usuario findByEmail(String email) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Usuario> criteria = cb.createQuery(Usuario.class);
		Root<Usuario> usuario = criteria.from(Usuario.class);
		// Swap criteria statements if you would like to try out type-safe criteria
		// queries, a new
		// feature in JPA 2.0
		// criteria.select(Usuario).where(cb.equal(Usuario.get(Usuario_.name), email));
		criteria.select(usuario).where(cb.equal(usuario.get("email"), email));
		return em.createQuery(criteria).getSingleResult();
	}
	
	 
	
	public List<Usuario> listaUsuarios() {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Usuario> criteria = cb.createQuery(Usuario.class);
		Root<Usuario> usuario = criteria.from(Usuario.class);

		criteria.select(usuario);
		
		List<Usuario> lista = em.createQuery(criteria).getResultList();
		
		em.close();
	  
		return lista;
	}
	
	
	public List<Usuario> listaTodos() {
		
		//CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Usuario> query = em.getCriteriaBuilder().createQuery(Usuario.class);
		query.select(query.from(Usuario.class));

		List<Usuario> lista = em.createQuery(query).getResultList();

		em.close();
		return lista;
	}
	
	
	
	public List<Usuario> findAllOrderedByName() {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Usuario> criteria = cb.createQuery(Usuario.class);
		Root<Usuario> usuario = criteria.from(Usuario.class);
		 
 
	 
		criteria.select(usuario).orderBy(cb.asc(usuario.get("name")));
		
		
		return em.createQuery(criteria).getResultList();
	}
	
	public List<UsuarioRest> lista() {
		
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<UsuarioRest> criteria = builder.createQuery(UsuarioRest.class);
		Root<UsuarioRest> root = criteria.from(UsuarioRest.class);
		
		 
		Path<Long> id = root.get(UsuarioRest_.id_user);
		Path<String> name = root.get(UsuarioRest_.name);
		Path<String> user = root.get(UsuarioRest_.user);
		Path<String> phoneNumber = root.get(UsuarioRest_.phoneNumber);
		Path<String> email = root.get(UsuarioRest_.email);
		Path<String> tipoAcesso = root.get(UsuarioRest_.tipoAcesso);
			 
		 
 		criteria.multiselect(id, name, user, phoneNumber, email, tipoAcesso);
 		
		return em.createQuery(criteria).getResultList();
	}
	
	public String convertStringToMd5(String valor) {
		   MessageDigest mDigest;
		   try { 
		      //Instanciamos o nosso HASH MD5, poderíamos usar outro como
		      //SHA, por exemplo, mas optamos por MD5.
		     mDigest = MessageDigest.getInstance("MD5");
		             
		     //Convert a String valor para um array de bytes em MD5
		     byte[] valorMD5 = mDigest.digest(valor.getBytes("UTF-8"));
		      
		     //Convertemos os bytes para hexadecimal, assim podemos salvar
		     //no banco para posterior comparação se senhas
		     StringBuffer sb = new StringBuffer();
		     for (byte b : valorMD5){
		            sb.append(Integer.toHexString((b & 0xFF) |
		            0x100).substring(1,3));
		     }
		 
		     return sb.toString();
		                 
		   } catch (NoSuchAlgorithmException e) {
		     // TODO Auto-generated catch block
		     e.printStackTrace();
		     return null;
		   } catch (UnsupportedEncodingException e) {
		     // TODO Auto-generated catch block
		     e.printStackTrace();
		     return null;
		  }
		}
	
	
}
