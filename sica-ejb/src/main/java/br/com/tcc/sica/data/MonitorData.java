package br.com.tcc.sica.data;

import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;

import br.com.tcc.sica.model.Agenda;
import br.com.tcc.sica.model.Monitor;
import br.com.tcc.sica.model.Monitor_;
import br.com.tcc.sica.model.StatusAgenda;
import br.com.tcc.sica.model.Usuario;

@ApplicationScoped
public class MonitorData {

	
	@Inject
	private Logger logger;

	@PersistenceContext
	static
	EntityManager em;
 
 
	
	private   static final String NIVEL_AGUA = "nivelDaAgua";
	private   static final String TEMP_SOLO = "temperaturaSolo";
	
	//Monitor monitor = new Monitor();

	@Transactional
	public void cadastroMonitor(Monitor monitor) {

		try {

			logger.info("Cadastro de monitoramento");

			em.persist(monitor);

		} catch (Exception e) {
			// TODO: handle exception
		}

	}
	
 
	public List<Monitor>  listaMonitoramento() {
  	
		  	CriteriaBuilder builder = em.getCriteriaBuilder();
		    CriteriaQuery<Monitor> cq = builder.createQuery(Monitor.class);
		    Root<Monitor> root = cq.from(Monitor.class);

		    cq.multiselect(root.get("barragem_nome"),builder.max(root.<Integer>get(NIVEL_AGUA)),builder.max(root.<Integer>get(TEMP_SOLO))).distinct(true);
		    cq.groupBy(root.get("barragem_nome"));
		    
		    List<Monitor> resultList = em.createQuery(cq).getResultList();
		    
		    return resultList;
 
	}
	 
	
	public List<Monitor> listaBarragens() {
	 
		Query q = em.createQuery("SELECT monitor.barragem_nome, MAX(monitor.nivelDaAgua), MAX(monitor.temperaturaSolo) FROM Monitor monitor GROUP BY monitor.barragem_nome");
		List<Monitor> result =  q.getResultList();
		 
			return result;
			 
		}
	
	
 
 
	}

