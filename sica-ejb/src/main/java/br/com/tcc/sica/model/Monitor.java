/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.tcc.sica.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "TB_MONITOR")

 
public class Monitor  implements Serializable {

	/** Default value included to remove warning. Remove or modify at will. **/
	private static final long serialVersionUID = 1L;

	 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	@Column(name = "id")
	private Integer id;

	
	@Size(min = 1, max = 75)
	@Column(name = "barragem_nome")
	private String barragem_nome;

	
	@Column(name = "sensor")
	private String sensor;

	
	@Column(name = "statusMonitora")
	private String statusMonitora;

	
	/// @Temporal(TemporalType.TIMESTAMP)
	private String dataMonitoramento;

	
	@Column(name = "nivelDaAgua")
	private Integer nivelDaAgua;

	
	@Column(name = "temperaturaSolo")
	private Integer temperaturaSolo;

	public Monitor() {
		
	}
	
	public Monitor(String barragem, Integer nivel,
			Integer temperatura) {
	 
		this.barragem_nome = barragem;
		this.nivelDaAgua = nivel;
		this.temperaturaSolo = temperatura;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBarragem_nome() {
		return barragem_nome;
	}

	public void setBarragem_nome(String barragem_nome) {
		this.barragem_nome = barragem_nome;
	}

	public String getSensor() {
		return sensor;
	}

	public void setSensor(String sensor) {
		this.sensor = sensor;
	}

	public String getStatusMonitora() {
		return statusMonitora;
	}

	public void setStatusMonitora(String statusMonitora) {
		this.statusMonitora = statusMonitora;
	}

	public String getDataMonitoramento() {
		return dataMonitoramento;
	}

	public void setDataMonitoramento(String dataMonitoramento) {
		this.dataMonitoramento = dataMonitoramento;
	}

	public Integer getNivelDaAgua() {
		return nivelDaAgua;
	}

	public void setNivelDaAgua(Integer nivelDaAgua) {
		this.nivelDaAgua = nivelDaAgua;
	}

	public Integer getTemperaturaSolo() {
		return temperaturaSolo;
	}

	public void setTemperaturaSolo(Integer temperaturaSolo) {
		this.temperaturaSolo = temperaturaSolo;
	}

//	@Override
//	public String toString() {
//		return String.format("%s[id=%d]", getClass().getSimpleName(), getId());
//	}
//
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + ((id == null) ? 0 : id.hashCode());
//		return result;
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		Monitor other = (Monitor) obj;
//		if (id == null) {
//			if (other.id != null)
//				return false;
//		} else if (!id.equals(other.id))
//			return false;
//		return true;
//	}

}
