package br.com.tcc.sica.data;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TransactionRequiredException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

//import br.com.caelum.livraria.dao.JPAUtil;
import br.com.tcc.sica.model.Insumo;

@ApplicationScoped
public class InsumoData {

	@Inject
	private Logger log;

	@PersistenceContext
	private EntityManager em;

	private List<Insumo> insumos;

	@Inject
	private Event<Insumo> insumoEventSrc;

	public Insumo findById(Long id) {
		return em.find(Insumo.class, id);
	}
	
	@Transactional
	public void registraInsumo(Insumo insumo) {

		try {

			log.info("Gravando Insumo: " + insumo.getNome());
			
		
			em.persist(insumo);
			insumoEventSrc.fire(insumo);
				
		} catch (Exception e) {
			System.out.println("Erro ao gravar Insumo  >>>> " + e.getMessage());
		}

	}
	
	@Transactional
	public void atualizaInsumo(Insumo insumo) {

		try {

			log.info("Atualizando Insumo: " + insumo.getNome());
			
		
			em.merge(insumo);
			 
				
		} catch (Exception e) {
			System.out.println("Erro ao gravar Insumo  >>>> " + e.getMessage());
		}

	}

	@Transactional
	public void removeInsumo(Insumo _insumoData) throws TransactionRequiredException {

		try {

			em.remove(em.getReference(_insumoData.getClass(), _insumoData.getId()));

		} catch (Exception e) {
			System.out.println("Erro ao remover Insumo  >>>> " + e.getMessage());
		}

	}

	public List<Insumo> getInsumos() {
		return insumos;

	}

	public List<Insumo> findAllOrderedByName() {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Insumo> criteria = cb.createQuery(Insumo.class);
		Root<Insumo> insumo = criteria.from(Insumo.class);

		criteria.select(insumo).orderBy(cb.asc(insumo.get("nome")));
		//System.out.println("#######################################" + em.createQuery(criteria).getResultList());
		return em.createQuery(criteria).getResultList();
	}

	@Override
	public String toString() {

		System.out.println();
		return "";
	}

}
