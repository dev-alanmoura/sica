/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.tcc.sica.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "TB_AGENDAMENTO", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
public class Agendamento implements Serializable {

	/** Default value included to remove warning. Remove or modify at will. **/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Size(min = 1, max = 45)
	@Column(name = "tipo")
	private String tipo;

	
	@OneToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name="insumo_id")
	private Insumo insumo;
	
	
	@JoinColumn(name="user_id")
	@OneToOne(optional = true, fetch = FetchType.LAZY) 
	private Usuario usuario;

	
	
	@OneToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name="agenda_id")
	private Agenda agenda;
	
	@NotNull
	@Column(name = "status")
	private String status;
	
	
	@JoinColumn(name="barragem_id")
	@OneToOne(optional = true, fetch = FetchType.LAZY)  
	private Barragem barragem;
	
	@Column(name = "descricao")
	private String descricao;

	
	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public Insumo getInsumo() {
		return insumo;
	}


	public void setInsumo(Insumo insumo) {
		this.insumo = insumo;
	}


	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	public Agenda getAgenda() {
		return agenda;
	
		
	}


	public void setAgenda(Agenda agenda) {
		this.agenda = agenda;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Barragem getBarragem() {
		return barragem;
	}


	public void setBarragem(Barragem barragem) {
		this.barragem = barragem;
	}
 
	
}
