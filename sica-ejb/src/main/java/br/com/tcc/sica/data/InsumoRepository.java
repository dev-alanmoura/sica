/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.tcc.sica.data;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.tcc.sica.model.Barragem;
import br.com.tcc.sica.model.Insumo;

@ApplicationScoped
public class InsumoRepository {

	@Inject
	private EntityManager em;
	
	private Barragem barragem;

	public Insumo findById(Long id) {
		return em.find(Insumo.class, id);
	}

	public Insumo findByEmail(String email) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Insumo> criteria = cb.createQuery(Insumo.class);
		Root<Insumo> Insumo = criteria.from(Insumo.class);
		// Swap criteria statements if you would like to try out type-safe criteria
		// queries, a new
		// feature in JPA 2.0
		// criteria.select(Insumo).where(cb.equal(Insumo.get(Insumo_.name), email));
		criteria.select(Insumo).where(cb.equal(Insumo.get("email"), email));
		return em.createQuery(criteria).getSingleResult();
	}

	public List<Insumo> findAllOrderedByName() {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Insumo> criteria = cb.createQuery(Insumo.class);
		Root<Insumo> Insumo = criteria.from(Insumo.class);
		// Swap criteria statements if you would like to try out type-safe criteria
		// queries, a new
		// feature in JPA 2.0
		// criteria.select(Insumo).orderBy(cb.asc(Insumo.get(Insumo_.name)));
		criteria.select(Insumo);
		return em.createQuery(criteria).getResultList();

	}
	

	

}
