package br.com.tcc.sica.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UsuarioRest.class)
public abstract class UsuarioRest_ {

	public static volatile SingularAttribute<UsuarioRest, String> password;
	public static volatile SingularAttribute<UsuarioRest, String> phoneNumber;
	public static volatile SingularAttribute<UsuarioRest, String> name;
	public static volatile SingularAttribute<UsuarioRest, Long> id_user;
	public static volatile SingularAttribute<UsuarioRest, String> user;
	public static volatile SingularAttribute<UsuarioRest, String> tipoAcesso;
	public static volatile SingularAttribute<UsuarioRest, String> email;

}

