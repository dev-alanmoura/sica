package br.com.tcc.sica.data;

import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import br.com.tcc.sica.model.Barragem;
import br.com.tcc.sica.model.Usuario;

@ApplicationScoped
public class BarragemData extends Persistence{

	@Inject
	private Logger log;

	@PersistenceContext
	private EntityManager em;
 
	
	@Transactional
	public void registraBarragem(Barragem barragem) {

		try {

			log.info("Gravando Barragem: " + barragem.getNome());
			em.persist(barragem);

		} catch (Exception e) {
			System.out.println("Erro ao gravar barragem  >>>> " + e.getMessage());
		}

	}
	
	
	public List<Barragem> findAllOrderedByName() {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Barragem> criteria = cb.createQuery(Barragem.class);
		Root<Barragem> barragem = criteria.from(Barragem.class);

		criteria.select(barragem).orderBy(cb.asc(barragem.get("nome")));
		return em.createQuery(criteria).getResultList();
	}
	
	
	public List<Barragem> listaBarragens() {

		List<Barragem> barragens =  em.createQuery("select n.longitude, n.latitude, n.categoriaDeRisco from Barragem n").getResultList();

		return barragens;
	}

	
	
	
}
