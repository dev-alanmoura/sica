package br.com.tcc.sica.data;

import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import br.com.tcc.sica.model.Agenda;
import br.com.tcc.sica.model.Insumo;
import br.com.tcc.sica.model.StatusAgenda;
import br.com.tcc.sica.model.Usuario;

@ApplicationScoped
public class AgendaData {

	
	@Inject
	private Logger logger;

	@PersistenceContext
	EntityManager em;

	Agenda agenda = new Agenda();

	@Transactional
	public void cadastroAgenda(Agenda agenda) {

		try {

			logger.info("Cadastro de agenda");

			em.persist(agenda);

		} catch (Exception e) {
			// TODO: handle exception
		}

	}
	
	
	@Transactional
	public void atualizaAgenda(Agenda agenda) {

		try {
			 
			logger.info("Atualiza de agenda");
			agenda.setStatusAgendamento(StatusAgenda.RESERVADO);
			em.merge(agenda);
			

		} catch (Exception e) {
			// TODO: handle exception
		}

	}
	

	public List<Agenda> listaDatasDisponiveis_old() {
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Agenda> criteria = cb.createQuery(Agenda.class);
		Root<Agenda> agenda = criteria.from(Agenda.class);

		criteria.select(agenda).orderBy(cb.asc(agenda.get("dataAgendamento")));
		System.out.println("#######################################" + em.createQuery(criteria).getResultList());
		return em.createQuery(criteria).getResultList();
	}
	

	
	public List<Agenda> listaDatasDisponiveis() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Agenda> criteria = cb.createQuery(Agenda.class);
		Root<Agenda> agenda = criteria.from(Agenda.class);
	
		criteria.select(agenda).where(cb.equal(agenda.get("statusAgendamento"), StatusAgenda.LIVRE));
		return em.createQuery(criteria).getResultList();
	}
	

}
