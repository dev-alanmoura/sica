package br.com.tcc.sica.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Agenda.class)
public abstract class Agenda_ {

	public static volatile SingularAttribute<Agenda, String> dataAgendamento;
	public static volatile SingularAttribute<Agenda, Long> id;
	public static volatile SingularAttribute<Agenda, StatusAgenda> statusAgendamento;

}

