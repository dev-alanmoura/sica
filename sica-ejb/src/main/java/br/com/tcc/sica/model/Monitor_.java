package br.com.tcc.sica.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Monitor.class)
public abstract class Monitor_ {

	public static volatile SingularAttribute<Monitor, String> barragem_nome;
	public static volatile SingularAttribute<Monitor, Integer> nivelDaAgua;
	public static volatile SingularAttribute<Monitor, String> statusMonitora;
	public static volatile SingularAttribute<Monitor, String> sensor;
	public static volatile SingularAttribute<Monitor, String> dataMonitoramento;
	public static volatile SingularAttribute<Monitor, Long> id;
	public static volatile SingularAttribute<Monitor, Integer> temperaturaSolo;

}

