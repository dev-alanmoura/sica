/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.tcc.sica.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@XmlRootElement
@Table(name = "TB_BARRAGEM", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
public class Barragem implements Serializable {

	/** Default value included to remove warning. Remove or modify at will. **/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Size(min = 1, max = 75)
	@Column(name = "nome")
	private String nome;

	@Column(name = "categoriaDeRisco")
	private String categoriaDeRisco;

	@Column(name = "potencialDeDano")
	private String potencialDeDano;

	@Column(name = "latitude")
	private String latitude;

	@Column(name = "longitude")
	private String longitude;

	@Column(name = "UF")
	private String UF;

	@Column(name = "codigoSensorMonitoramento")
	private String codigoSensorMonitoramento;

	@Column(name = "statusMonitoramento")
	private String statusMonitoramento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCategoria() {
		return categoriaDeRisco;
	}

	public void setCategoria(String categoria) {
		this.categoriaDeRisco = categoria;
	}

	public String getUF() {
		return UF;
	}

	public void setUF(String uF) {
		UF = uF;
	}

	public String getPotencialDeDano() {
		return potencialDeDano;
	}

	public void setPotencialDeDano(String potencialDeDano) {
		this.potencialDeDano = potencialDeDano;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getCodigoSensorMonitoramento() {
		return codigoSensorMonitoramento;
	}

	public void setCodigoSensorMonitoramento(String codigoSensorMonitoramento) {
		this.codigoSensorMonitoramento = codigoSensorMonitoramento;
	}

	public String getCategoriaDeRisco() {
		return categoriaDeRisco;
	}

	public void setCategoriaDeRisco(String categoriaDeRisco) {
		this.categoriaDeRisco = categoriaDeRisco;
	}

	public String getStatusMonitoramento() {
		return statusMonitoramento;
	}

	public void setStatusMonitoramento(String statusMonitoramento) {
		this.statusMonitoramento = statusMonitoramento;
	}

	@Override
	public String toString() {
		return String.format("%s[id=%d]", getClass().getSimpleName(), getId());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Barragem other = (Barragem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
