package br.com.tcc.sica.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Barragem.class)
public abstract class Barragem_ {

	public static volatile SingularAttribute<Barragem, String> UF;
	public static volatile SingularAttribute<Barragem, String> codigoSensorMonitoramento;
	public static volatile SingularAttribute<Barragem, String> latitude;
	public static volatile SingularAttribute<Barragem, String> potencialDeDano;
	public static volatile SingularAttribute<Barragem, String> nome;
	public static volatile SingularAttribute<Barragem, String> categoriaDeRisco;
	public static volatile SingularAttribute<Barragem, Long> id;
	public static volatile SingularAttribute<Barragem, String> longitude;
	public static volatile SingularAttribute<Barragem, String> statusMonitoramento;

}

