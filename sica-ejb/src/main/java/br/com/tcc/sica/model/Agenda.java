package br.com.tcc.sica.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
 
 
@Entity
@XmlRootElement
@Table(name = "TB_AGENDA", uniqueConstraints = @UniqueConstraint(columnNames ="id"))
public class Agenda implements Serializable {
	

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@Column(name = "dataAgendamento")
	//@Temporal(TemporalType.DATE)
   // Calendar dataAgendamento =  Calendar.getInstance();
	
	private String dataAgendamento;
	
	@NotNull
	@Column(name = "statusAgendamento")
	@Enumerated(EnumType.STRING)
	private StatusAgenda statusAgendamento = StatusAgenda.LIVRE;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
 
	public String getDataAgendamento() {
		return dataAgendamento;
	}

	public void setDataAgendamento(String dataAgendamento) {
		this.dataAgendamento = dataAgendamento;
	}

	public StatusAgenda getStatusAgendamento() {
		return statusAgendamento;
	}

	public void setStatusAgendamento(StatusAgenda statusAgendamento) {
		this.statusAgendamento = statusAgendamento;
		
	}
	
	@Override
	public String toString() {
	    return String.format("%s[id=%d]", getClass().getSimpleName(), getId());
	}

	
	
	public String getDataFormatada(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(this.dataAgendamento);
}
	
	private  Calendar parseData( ) { 
		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(this.dataAgendamento.toString());
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			return calendar;
		} catch (ParseException e) {
			throw new IllegalArgumentException(e);
		}
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Agenda other = (Agenda) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
} 	
